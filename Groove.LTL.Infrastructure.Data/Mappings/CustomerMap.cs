﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

using Groove.LTL.Domain.Customer.Models;
using Groove.LTL.Infrastructure.Data.Extensions;


namespace Groove.LTL.Infrastructure.Data.Mappings
{
    /// <summary>
    /// Customer Map Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Infrastructure.Data.Extensions.EntityTypeConfiguration{Groove.LTL.Domain.Customer.Models.CustomerModel}" />
    public class CustomerMap : EntityTypeConfiguration<CustomerModel>
    {
        /// <summary>
        /// Maps the specified builder.
        /// </summary>
        /// <param name="builder">The builder.</param>
        public override void Map(EntityTypeBuilder<CustomerModel> builder)
        {
            builder.Property(c => c.Id)
                .HasColumnName("Id");

            builder.Property(c => c.CompanyName)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.ContactName)
                .HasColumnType("varchar(100)")
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(c => c.Email)
                .HasColumnType("varchar(100)")
                .HasMaxLength(11)
                .IsRequired();
        }
    }
}
