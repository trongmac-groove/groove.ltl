﻿using System.Threading.Tasks;

namespace Groove.LTL.Infrastructure.Identity.Services
{
    /// <summary>
    /// SMS Sender Interface
    /// </summary>
    public interface ISmsSender
    {
        /// <summary>
        /// Sends the SMS asynchronous.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        Task SendSmsAsync(string number, string message);
    }
}
