﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

using AutoMapper;

using Groove.LTL.Application.Customer.Interfaces;
using Groove.LTL.Application.Customer.Services;
using Groove.LTL.Domain.Core.Events;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;
using Groove.LTL.Domain.Customer.Commands;
using Groove.LTL.Domain.Customer.Commands.Handlers;
using Groove.LTL.Domain.Customer.Events;
using Groove.LTL.Domain.Customer.Events.Handlers;
using Groove.LTL.Domain.Customer.Repositories;
using Groove.LTL.Infrastructure.Data.Contexts;
using Groove.LTL.Infrastructure.Data.Repositories.Customer;
using Groove.LTL.Infrastructure.Data.UnitOfWork;
using Groove.LTL.Infrastructure.Identity.Authorization;
using Groove.LTL.Domain.Core.Bus;

namespace Groove.LTL.Infrastructure.IoC
{
    /// <summary>
    /// Simple Injector BootStrapper Class
    /// </summary>
    public class SimpleInjectorBootStrapper
    {
        /// <summary>
        /// Registers the services.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void RegisterServices(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // ASP.NET Authorization Polices
            services.AddSingleton<IAuthorizationHandler, ClaimsRequirementHandler>(); ;

            // Application
            services.AddSingleton(Mapper.Configuration);
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<ICustomerService, CustomerService>();

            // Domain - Events
            services.AddScoped<IDomainNotificationHandler<DomainNotification>, DomainNotificationHandler>();
            services.AddScoped<IMessageHandler<CustomerCreatedEvent>, CustomerEventHandler>();
            services.AddScoped<IMessageHandler<CustomerUpdatedEvent>, CustomerEventHandler>();
            services.AddScoped<IMessageHandler<CustomerDeletedEvent>, CustomerEventHandler>();

            // Domain - Commands
            services.AddScoped<IMessageHandler<CreateCustomerCommand>, CustomerCommandHandler>();
            services.AddScoped<IMessageHandler<UpdateCustomerCommand>, CustomerCommandHandler>();
            services.AddScoped<IMessageHandler<DeleteCustomerCommand>, CustomerCommandHandler>();

            // Infra - Data
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<LtlContext>();

            services.AddScoped<IBus, InMemoryBus>();
        }
    }
}
