﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace Groove.LTL.Infrastructure.Identity.Models.ManageViewModels
{
    /// <summary>
    /// Configure Two Factor ViewModel Class
    /// </summary>
    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }

        public ICollection<SelectListItem> Providers { get; set; }
    }
}
