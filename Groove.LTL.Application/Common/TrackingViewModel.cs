﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Groove.LTL.Application.Common
{
    public class TrackingViewModel
    {
        [Key]
        public int Id { get; protected set; }
        [Timestamp]
        public string RowVersion { get; set; }
        public string CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
