﻿using System;

using Groove.LTL.Domain.Customer.Validations;

namespace Groove.LTL.Domain.Customer.Commands
{
    /// <summary>
    /// Update Customer Command Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Commands.CustomerCommand" />
    public class UpdateCustomerCommand : CustomerCommand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateCustomerCommand"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="contactName">Name of the contact.</param>
        /// <param name="email">The email.</param>
        public UpdateCustomerCommand(int id, string companyName, string contactName, string email)
        {
            Id = id;
            CompanyName = companyName;
            ContactName = contactName;
            Email = email;
        }

        /// <summary>
        /// Returns true if update customer command is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValid()
        {
            ValidationResult = new UpdateCustomerValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}
