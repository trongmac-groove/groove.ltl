﻿using Groove.LTL.Domain.Core.Commands;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Infrastructure.Data.Contexts;

namespace Groove.LTL.Infrastructure.Data.UnitOfWork
{
    /// <summary>
    /// Unit of Work Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Interfaces.IUnitOfWork" />
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LtlContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public UnitOfWork(LtlContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns></returns>
        public CommandResponse Commit()
        {
            var rowsAffected = _context.SaveChanges();

            return new CommandResponse(rowsAffected > 0);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
