﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Groove.LTL.Infrastructure.Identity.Models
{
    /// <summary>
    /// Adds profile data for application users by adding properties to the Application User class
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUser" />
    public class ApplicationUser : IdentityUser
    {

    }
}
