﻿using System;

using Groove.LTL.Domain.Core.Commands;

namespace Groove.LTL.Domain.Customer.Commands
{
    /// <summary>
    /// Customer Command Class Base
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Commands.Command" />
    public abstract class CustomerCommand : Command
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string Email { get; set; }
    }
}
