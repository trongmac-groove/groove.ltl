﻿using System;
using System.Collections.Generic;

using AutoMapper;

using Groove.LTL.Application.Customer.Interfaces;
using Groove.LTL.Application.Customer.ViewModels;
using Groove.LTL.Domain.Core.Bus;
using Groove.LTL.Domain.Customer.Commands;
using Groove.LTL.Domain.Customer.Repositories;

namespace Groove.LTL.Application.Customer.Services
{
    /// <summary>
    /// Customer service class
    /// </summary>
    /// <seealso cref="Groove.LTL.Application.Customer.Interfaces.ICustomerService" />
    public class CustomerService : ICustomerService
    {
        #region Private Properties
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepository;
        private readonly IBus _bus;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerService"/> class.
        /// </summary>
        /// <param name="mapper">The mapper.</param>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="bus">The bus.</param>
        public CustomerService(IMapper mapper, ICustomerRepository customerRepository, IBus bus)
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
            _bus = bus;
        }

        /// <summary>
        /// Gets all customer view models.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CustomerViewModel> GetAll()
        {
            return _mapper.Map<IEnumerable<CustomerViewModel>>(_customerRepository.GetAll());
        }

        /// <summary>
        /// Gets the customer view model by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public CustomerViewModel GetById(int id)
        {
            return _mapper.Map<CustomerViewModel>(_customerRepository.GetById(id));
        }

        /// <summary>
        /// Creates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        public void Create(CustomerViewModel customerViewModel)
        {
            var createCommand = _mapper.Map<CreateCustomerCommand>(customerViewModel);
            _bus.SendCommand(createCommand);
        }

        /// <summary>
        /// Updates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        public void Update(CustomerViewModel customerViewModel)
        {
            var updateCommand = _mapper.Map<UpdateCustomerCommand>(customerViewModel);
            _bus.SendCommand(updateCommand);
        }

        /// <summary>
        /// Deletes customer by the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public void Delete(int id)
        {
            var deleteCommand = new DeleteCustomerCommand(id);
            _bus.SendCommand(deleteCommand);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
