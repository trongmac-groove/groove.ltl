﻿using Groove.LTL.Domain.Core.Models;

namespace Groove.LTL.Domain.Core.Interfaces
{
    /// <summary>
    /// Handler Inteface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMessageHandler<in T> where T : Message
    {
        /// <summary>
        /// Handles the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Handle(T message);
    }
}
