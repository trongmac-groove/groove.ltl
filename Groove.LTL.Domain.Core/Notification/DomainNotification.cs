﻿using System;

using Groove.LTL.Domain.Core.Events;

namespace Groove.LTL.Domain.Core.Notification
{
    /// <summary>
    /// Domain Notification Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Events.Event" />
    public class DomainNotification : Event
    {
        #region Properties
        public Guid DomainNotificationId { get; private set; }

        public string Key { get; private set; }

        public string Value { get; private set; }

        public int Version { get; private set; }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainNotification"/> class.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public DomainNotification(string key, string value)
        {
            DomainNotificationId = Guid.NewGuid();
            Version = 1;
            Key = key;
            Value = value;
        }
    }
}
