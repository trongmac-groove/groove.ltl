﻿using System;

namespace Groove.LTL.Domain.Customer.Events
{
    /// <summary>
    /// Customer Updated Event Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Events.CustomerEvent" />
    public class CustomerUpdatedEvent : CustomerEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerUpdatedEvent"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="contactName">Name of the contact.</param>
        /// <param name="email">The email.</param>
        public CustomerUpdatedEvent(int id, string companyName, string contactName, string email)
        {
            Id = id;
            CompanyName = companyName;
            ContactName = contactName;
            Email = email;
        }
    }
}
