﻿using System;

namespace Groove.LTL.Domain.Core.Models
{
    /// <summary>
    /// Message Class Base
    /// </summary>
    public abstract class Message
    {
        public string MessageType { get; protected set; }

        public int AggregateId { get; protected set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class.
        /// </summary>
        protected Message()
        {
            MessageType = GetType().Name;
        }
    }
}
