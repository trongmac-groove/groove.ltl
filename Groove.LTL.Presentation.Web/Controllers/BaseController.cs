﻿using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;
using Microsoft.AspNetCore.Mvc;

namespace Groove.LTL.Presentation.Web.Controllers
{
    public class BaseController : Controller
    {
        private readonly IDomainNotificationHandler<DomainNotification> _notifications;

        public BaseController(IDomainNotificationHandler<DomainNotification> notifications)
        {
            _notifications = notifications;
        }

        public bool IsValidOperation()
        {
            return (!_notifications.HasNotifications());
        }
    }
}
