﻿using System;
using System.Collections.Generic;

using Groove.LTL.Application.Customer.ViewModels;

namespace Groove.LTL.Application.Customer.Interfaces
{
    /// <summary>
    /// Customer Service Interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface ICustomerService : IDisposable
    {
        /// <summary>
        /// Gets all customer view models.
        /// </summary>
        /// <returns></returns>
        IEnumerable<CustomerViewModel> GetAll();

        /// <summary>
        /// Gets the customer view model by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        CustomerViewModel GetById(int id);

        /// <summary>
        /// Creates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        void Create(CustomerViewModel customerViewModel);

        /// <summary>
        /// Updates the specified customer view model.
        /// </summary>
        /// <param name="customerViewModel">The customer view model.</param>
        void Update(CustomerViewModel customerViewModel);

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        void Delete(int id);
    }
}
