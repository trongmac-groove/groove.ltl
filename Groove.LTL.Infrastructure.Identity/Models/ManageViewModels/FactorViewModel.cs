﻿namespace Groove.LTL.Infrastructure.Identity.Models.ManageViewModels
{
    /// <summary>
    /// Factor ViewModel Class
    /// </summary>
    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }
}
