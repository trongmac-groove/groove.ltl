﻿using System;

using Groove.LTL.Domain.Core.Commands;

namespace Groove.LTL.Domain.Core.Interfaces
{
    /// <summary>
    /// Unit of Work Interface
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns></returns>
        CommandResponse Commit();
    }
}
