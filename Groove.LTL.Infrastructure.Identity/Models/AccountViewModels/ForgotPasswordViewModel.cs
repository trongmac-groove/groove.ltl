﻿using System.ComponentModel.DataAnnotations;

namespace Groove.LTL.Infrastructure.Identity.Models.AccountViewModels
{
    /// <summary>
    /// Forgot Password ViewModel Class
    /// </summary>
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
