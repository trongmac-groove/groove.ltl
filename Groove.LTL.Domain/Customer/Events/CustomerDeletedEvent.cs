﻿using System;

namespace Groove.LTL.Domain.Customer.Events
{
    /// <summary>
    /// Customer Deleted Event Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Events.CustomerEvent" />
    public class CustomerDeletedEvent : CustomerEvent
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerDeletedEvent"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="aggregateId">The Aggregate Id.</param>
        public CustomerDeletedEvent(int id)
        {
            Id = id;
            AggregateId = id;
        }
    }
}
