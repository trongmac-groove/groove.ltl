﻿using System;

using Groove.LTL.Domain.Customer.Validations;

namespace Groove.LTL.Domain.Customer.Commands
{
    /// <summary>
    /// Delete Customer Command Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Commands.CustomerCommand" />
    public class DeleteCustomerCommand : CustomerCommand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteCustomerCommand"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public DeleteCustomerCommand(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Returns true if delete customer command is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValid()
        {
            ValidationResult = new DeleteCustomerValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}
