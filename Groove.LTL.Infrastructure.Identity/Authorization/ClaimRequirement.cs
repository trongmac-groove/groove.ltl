﻿using Microsoft.AspNetCore.Authorization;

namespace Groove.LTL.Infrastructure.Identity.Authorization
{
    /// <summary>
    /// Claim Requirement Class
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Authorization.IAuthorizationRequirement" />
    public class ClaimRequirement : IAuthorizationRequirement
    {
        public string ClaimName { get; set; }

        public string ClaimValue { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClaimRequirement"/> class.
        /// </summary>
        /// <param name="claimName">Name of the claim.</param>
        /// <param name="claimValue">The claim value.</param>
        public ClaimRequirement(string claimName, string claimValue)
        {
            ClaimName = claimName;
            ClaimValue = claimValue;
        }
    }
}
