﻿using Groove.LTL.Domain.Core.Models;
using System;

namespace Groove.LTL.Domain.Customer.Models
{
    public class TrackingEntity : Entity
    {
        public TrackingEntity()
        {
            CreatedDate = DateTime.Now;
        }

        public string CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}