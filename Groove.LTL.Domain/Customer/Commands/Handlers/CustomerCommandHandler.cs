﻿using System;

using Groove.LTL.Domain.Core;
using Groove.LTL.Domain.Core.Bus;
using Groove.LTL.Domain.Core.Commands;
using Groove.LTL.Domain.Core.Events;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;
using Groove.LTL.Domain.Customer.Events;
using Groove.LTL.Domain.Customer.Models;
using Groove.LTL.Domain.Customer.Repositories;

namespace Groove.LTL.Domain.Customer.Commands.Handlers
{
    /// <summary>
    /// Customer Command Handler Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Commands.CommandHandler" />
    /// <seealso cref="IMessageHandler{T}" />
    /// <seealso cref="IMessageHandler{T}" />
    /// <seealso cref="IMessageHandler{T}" />
    public class CustomerCommandHandler : CommandHandler, IMessageHandler<CreateCustomerCommand>, IMessageHandler<UpdateCustomerCommand>, IMessageHandler<DeleteCustomerCommand>
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IBus _bus;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerCommandHandler"/> class.
        /// </summary>
        /// <param name="customerRepository">The customer repository.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="bus">The bus.</param>
        /// <param name="notification">The notification.</param>
        public CustomerCommandHandler(ICustomerRepository customerRepository, IUnitOfWork unitOfWork, IBus bus, IDomainNotificationHandler<DomainNotification> notification) : base(unitOfWork, bus, notification)
        {
            _customerRepository = customerRepository;
            _bus = bus;
        }

        /// <summary>
        /// Handles the create customer message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CreateCustomerCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return;
            }

            var customer = new CustomerModel(message.CompanyName, message.ContactName, message.Email);

            _customerRepository.Add(customer);

            if (Commit())
            {
                _bus.RaiseEvent(new CustomerCreatedEvent(customer.Id, customer.CompanyName, customer.ContactName, customer.Email));
            }
        }

        /// <summary>
        /// Handles the update customer message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(UpdateCustomerCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return;
            }

            var customer = new CustomerModel(message.CompanyName, message.ContactName, message.Email);

            _customerRepository.Update(customer);

            if (Commit())
            {
                _bus.RaiseEvent(new CustomerUpdatedEvent(customer.Id, customer.CompanyName, customer.ContactName, customer.Email));
            }
        }

        /// <summary>
        /// Handles the delete customer message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(DeleteCustomerCommand message)
        {
            if (!message.IsValid())
            {
                NotifyValidationErrors(message);
                return;
            }

            _customerRepository.Remove(message.Id);

            if (Commit())
            {
                _bus.RaiseEvent(new CustomerDeletedEvent(message.Id));
            }
        }
    }
}
