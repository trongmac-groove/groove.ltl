﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Groove.LTL.Infrastructure.Data.Contexts;

namespace Groove.LTL.Infrastructure.Data.Migrations
{
    [DbContext(typeof(LtlContext))]
    [Migration("20170407100507_InitData")]
    partial class InitData
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Groove.LTL.Domain.Customer.Models.CustomerModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CompanyName");

                    b.Property<string>("ContactName");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("CreatedUser");

                    b.Property<string>("Email");

                    b.Property<byte[]>("RowVersion")
                        .IsConcurrencyToken()
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<string>("UpdatedUser");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });
        }
    }
}
