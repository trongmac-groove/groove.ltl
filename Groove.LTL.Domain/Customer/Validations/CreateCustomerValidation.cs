﻿using Groove.LTL.Domain.Customer.Commands;

namespace Groove.LTL.Domain.Customer.Validations
{
    /// <summary>
    /// Create Customer Validation Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Validations.CustomerValidation{CreateCustomerCommand}" />
    public class CreateCustomerValidation : CustomerValidation<CreateCustomerCommand>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateCustomerValidation"/> class.
        /// </summary>
        public CreateCustomerValidation()
        {
            ValidateCompanyName();
            ValidateContactName();
            ValidateEmail();
        }
    }
}
