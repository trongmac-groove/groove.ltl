﻿using System.ComponentModel.DataAnnotations;

namespace Groove.LTL.Infrastructure.Identity.Models.ManageViewModels
{
    /// <summary>
    /// Add Phone Number ViewModel Class
    /// </summary>
    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }
}
