﻿using Groove.LTL.Domain.Core.Events;
using Groove.LTL.Domain.Core.Interfaces;

namespace Groove.LTL.Domain.Customer.Events.Handlers
{
    /// <summary>
    /// Customer Event Handler Class
    /// </summary>
    /// <seealso cref="IMessageHandler{T}" />
    /// <seealso cref="IMessageHandler{T}" />
    /// <seealso cref="IMessageHandler{T}" />
    public class CustomerEventHandler : IMessageHandler<CustomerCreatedEvent>, IMessageHandler<CustomerUpdatedEvent>, IMessageHandler<CustomerDeletedEvent>
    {
        /// <summary>
        /// Handles customer created event message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CustomerCreatedEvent message)
        {

        }

        /// <summary>
        /// Handles customer updated  event message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CustomerUpdatedEvent message)
        {

        }

        /// <summary>
        /// Handles customer deleted event message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Handle(CustomerDeletedEvent message)
        {

        }
    }
}
