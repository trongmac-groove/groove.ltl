﻿using Groove.LTL.Domain.Core.Bus;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Notification;

namespace Groove.LTL.Domain.Core.Commands
{
    /// <summary>
    /// Command Handler Class
    /// </summary>
    public class CommandHandler
    {
        #region Private Variables
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBus _bus;
        private readonly IDomainNotificationHandler<DomainNotification> _notification;
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandHandler"/> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="bus">The bus.</param>
        /// <param name="notification">The notification.</param>
        public CommandHandler(IUnitOfWork unitOfWork, IBus bus, IDomainNotificationHandler<DomainNotification> notification)
        {
            _unitOfWork = unitOfWork;
            _notification = notification;
            _bus = bus;
        }

        /// <summary>
        /// Notifies the validation errors.
        /// </summary>
        /// <param name="message">The message.</param>
        protected void NotifyValidationErrors(Command message)
        {
            foreach (var error in message.ValidationResult.Errors)
            {
                _bus.RaiseEvent(new DomainNotification(message.MessageType, error.ErrorMessage));
            }
        }

        /// <summary>
        /// Commits this instance.
        /// </summary>
        /// <returns></returns>
        public bool Commit()
        {
            if (_notification.HasNotifications())
            {
                return false;
            }

            var commandResponse = _unitOfWork.Commit();

            if (commandResponse.Success)
            {
                return true;
            }

            _bus.RaiseEvent(new DomainNotification("Commit", "We had a problem during saving your data."));

            return false;
        }
    }
}
