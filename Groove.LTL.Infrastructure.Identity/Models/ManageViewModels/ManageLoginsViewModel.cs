﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;

namespace Groove.LTL.Infrastructure.Identity.Models.ManageViewModels
{
    /// <summary>
    /// Manage Logins ViewModel Class
    /// </summary>
    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }

        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }
}
