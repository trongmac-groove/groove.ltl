﻿using System;
using Groove.LTL.Application.Common;

namespace Groove.LTL.Application.Customer.ViewModels
{
    /// <summary>
    /// Customer View Model Class
    /// </summary>
    public class CustomerViewModel : TrackingViewModel
    {
        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public  string Email { get; set; }
    }
}
