﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Groove.LTL.Application.Common;
using Groove.LTL.Application.Customer.ViewModels;
using Groove.LTL.Application.Utilities;
using Groove.LTL.Domain.Customer.Models;
using Groove.LTL.Domain.Customer.Commands;

namespace Groove.LTL.Application.AutoMapper
{
    public class AutoMapperConfiguration
    {
        /// <summary>
        /// Registers the mappings.
        /// </summary>
        /// <returns></returns>
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DomainToViewModelProfile());
                cfg.AddProfile(new ViewModelToDomainProfile());
                cfg.AddProfile(new MappingProfile());
            });
        }
    }

    public class DomainToViewModelProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainToViewModelProfile"/> class.
        /// </summary>
        public DomainToViewModelProfile()
        {
            CreateMap<CustomerModel, CustomerViewModel>();
        }
    }

    public class ViewModelToDomainProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModelToDomainProfile"/> class.
        /// </summary>
        public ViewModelToDomainProfile()
        {
            CreateMap<CustomerViewModel, CreateCustomerCommand>()
                .ConstructUsing(c => new CreateCustomerCommand(c.CompanyName, c.ContactName, c.Email));

            CreateMap<CustomerViewModel, UpdateCustomerCommand>()
                .ConstructUsing(c => new UpdateCustomerCommand(c.Id, c.CompanyName, c.ContactName, c.Email));
        }
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            var types = typeof(ICreateMapping).GetTypeInfo().Assembly.GetTypes();

            LoadEntityMappings(types);
            //LoadStandardMappings(types);
        }

        private void LoadEntityMappings(IEnumerable<Type> types)
        {
            // Define mapping for base class
            CreateMap<TrackingViewModel, TrackingEntity>().ForMember(x => x.RowVersion, c => c.MapFrom(dto => ByteArrayConverter.FromString(dto.RowVersion)));
            CreateMap<TrackingEntity, TrackingViewModel>().ForMember(x => x.RowVersion, c => c.MapFrom(entity => ByteArrayConverter.ToString(entity.RowVersion)));

            var maps = (from t in types
                where typeof(ICreateMapping).IsAssignableFrom(t)
                      && !t.GetTypeInfo().IsAbstract
                      && !t.GetTypeInfo().IsInterface
                select (ICreateMapping)Activator.CreateInstance(t)).ToArray();

            foreach (var map in maps)
            {
                map.CreateMapping(this);
            }
        }
    }

    public interface ICreateMapping
    {
        /// <summary>
        /// Defines the mapping and adds it into the mapping profile
        /// </summary>
        /// <param name="profile"></param>
        void CreateMapping(Profile profile);
    }
}
