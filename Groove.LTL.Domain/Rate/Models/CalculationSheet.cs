﻿using Groove.LTL.Domain.Customer.Models;

namespace Groove.LTL.Domain.Rate.Models
{
    public class CalculationSheet : TrackingEntity
    {
        public string CalculationSheetId { get; set; }
    }
}
