﻿using System;

using FluentValidation;

using Groove.LTL.Domain.Customer.Commands;

namespace Groove.LTL.Domain.Customer.Validations
{
    /// <summary>
    /// Customer Validation Class Base
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="FluentValidation.AbstractValidator{T}" />
    public abstract class CustomerValidation<T> : AbstractValidator<T> where T : CustomerCommand
    {
        /// <summary>
        /// Validates the identifier.
        /// </summary>
        protected void ValidateId()
        {
            RuleFor(c => c.Id)
                .NotEqual(0);
        }

        /// <summary>
        /// Validates the name of the company.
        /// </summary>
        protected void ValidateCompanyName()
        {
            RuleFor(c => c.CompanyName)
                .NotEmpty().WithMessage("Please ensure you have entered the CompanyName")
                .Length(2, 150).WithMessage("The CompanyName must have between 2 and 150 characters");
        }

        /// <summary>
        /// Validates the name of the contact.
        /// </summary>
        protected void ValidateContactName()
        {
            RuleFor(c => c.ContactName)
                .NotEmpty().WithMessage("Please ensure you have entered the ContactName")
                .Length(2, 150).WithMessage("The ContactName must have between 2 and 150 characters");
        }

        /// <summary>
        /// Validates the email.
        /// </summary>
        protected void ValidateEmail()
        {
            RuleFor(c => c.Email)
                .NotEmpty().WithMessage("Please ensure you have entered the Email");
        }
    }
}
