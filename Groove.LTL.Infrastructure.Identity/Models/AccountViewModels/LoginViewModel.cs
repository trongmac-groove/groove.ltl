﻿using System.ComponentModel.DataAnnotations;

namespace Groove.LTL.Infrastructure.Identity.Models.AccountViewModels
{
    /// <summary>
    /// Login ViewModel Class
    /// </summary>
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
