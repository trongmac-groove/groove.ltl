﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc.Rendering;

namespace Groove.LTL.Infrastructure.Identity.Models.AccountViewModels
{
    /// <summary>
    /// Send Code ViewModel Class
    /// </summary>
    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }

        public ICollection<SelectListItem> Providers { get; set; }

        public string ReturnUrl { get; set; }

        public bool RememberMe { get; set; }
    }
}
