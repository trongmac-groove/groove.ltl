﻿using Groove.LTL.Domain.Core.Models;
using System;

namespace Groove.LTL.Domain.Core.Events
{
    /// <summary>
    /// Event Class Base
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Models.Message" />
    public abstract class Event : Message
    {
        public DateTime Timestamp { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Event"/> class.
        /// </summary>
        protected Event()
        {
            Timestamp = DateTime.Now;
        }
    }
}
