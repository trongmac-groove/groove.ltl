﻿using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Customer.Models;

namespace Groove.LTL.Domain.Customer.Repositories
{
    /// <summary>
    /// Customer Repository Interface
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Interfaces.IRepository{CustomerModel}" />
    public interface ICustomerRepository : IRepository<CustomerModel>
    {

    }
}
