﻿using System;
using System.Collections.Generic;
using System.Text;
using Groove.LTL.Domain.Core.Commands;
using Groove.LTL.Domain.Core.Events;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Models;

namespace Groove.LTL.Domain.Core.Bus
{
    public sealed class InMemoryBus : IBus
    {
        public static Func<IServiceProvider> ContainerAccessor { get; set; }
        private static IServiceProvider Container => ContainerAccessor();

        public void RaiseEvent<T>(T theEvent) where T : Event
        {
            Publish(theEvent);
        }

        public void SendCommand<T>(T theCommand) where T : Command
        {
            Publish(theCommand);
        }

        private static void Publish<T>(T message) where T : Message
        {
            if (Container == null) return;

            var obj = Container.GetService(message.MessageType.Equals("DomainNotification")
                ? typeof(IDomainNotificationHandler<T>)
                : typeof(IMessageHandler<T>));

            ((IMessageHandler<T>)obj).Handle(message);
        }

        private object GetService(Type serviceType)
        {
            return Container.GetService(serviceType);
        }

        private T GetService<T>()
        {
            return (T)Container.GetService(typeof(T));
        }
    }
}
