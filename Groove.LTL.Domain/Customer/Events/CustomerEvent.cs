﻿using System;

using Groove.LTL.Domain.Core.Events;

namespace Groove.LTL.Domain.Customer.Events
{
    /// <summary>
    /// Customer Event Class Base 
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Core.Events.Event" />
    public abstract class CustomerEvent : Event
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }

        public string ContactName { get; set; }

        public string Email { get; set; }
    }
}
