﻿using System.Collections.Generic;
using System.Linq;

using Groove.LTL.Domain.Core.Events;
using Groove.LTL.Domain.Core.Interfaces;
using Groove.LTL.Domain.Core.Models;

namespace Groove.LTL.Domain.Core.Interfaces
{
    /// <summary>
    /// Domain Notification Handler Interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <seealso cref="IMessageHandler{T}" />
    public interface IDomainNotificationHandler<T> : IMessageHandler<T> where T : Message
    {
        /// <summary>
        /// Determines whether this instance has notifications.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance has notifications; otherwise, <c>false</c>.
        /// </returns>
        bool HasNotifications();

        /// <summary>
        /// Gets the notifications.
        /// </summary>
        /// <returns></returns>
        List<T> GetNotifications();
    }
}
