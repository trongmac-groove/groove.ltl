﻿using Groove.LTL.Domain.Customer.Repositories;
using Groove.LTL.Infrastructure.Data.Contexts;

namespace Groove.LTL.Infrastructure.Data.Repositories.Customer
{
    /// <summary>
    /// Customer Repository Class
    /// </summary>
    public class CustomerRepository : Repository<Domain.Customer.Models.CustomerModel>, ICustomerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerRepository"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public CustomerRepository(LtlContext context)
            :base(context)
        {

        }
    }
}
