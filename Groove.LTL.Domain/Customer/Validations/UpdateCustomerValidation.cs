﻿using Groove.LTL.Domain.Customer.Commands;

namespace Groove.LTL.Domain.Customer.Validations
{
    /// <summary>
    /// Update Customer Validation Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Validations.CustomerValidation{UpdateCustomerCommand}" />
    public class UpdateCustomerValidation : CustomerValidation<UpdateCustomerCommand>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateCustomerValidation"/> class.
        /// </summary>
        public UpdateCustomerValidation()
        {
            ValidateId();
            ValidateCompanyName();
            ValidateContactName();
            ValidateEmail();
        }
    }
}
