﻿using System.ComponentModel.DataAnnotations;

namespace Groove.LTL.Infrastructure.Identity.Models.AccountViewModels
{
    /// <summary>
    /// External Login Confirmation ViewModel Class
    /// </summary>
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
