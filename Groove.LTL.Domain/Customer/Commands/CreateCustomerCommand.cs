﻿using Groove.LTL.Domain.Customer.Validations;

namespace Groove.LTL.Domain.Customer.Commands
{
    /// <summary>
    /// Create Customer Command Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Commands.CustomerCommand" />
    public class CreateCustomerCommand : CustomerCommand
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateCustomerCommand"/> class.
        /// </summary>
        /// <param name="companyName">Name of the company.</param>
        /// <param name="contactName">Name of the contact.</param>
        /// <param name="email">The email.</param>
        public CreateCustomerCommand(string companyName, string contactName, string email)
        {
            CompanyName = companyName;
            ContactName = contactName;
            Email = email;
        }

        /// <summary>
        /// Returns true if create customer command is valid.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </returns>
        public override bool IsValid()
        {
            ValidationResult = new CreateCustomerValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }
}
