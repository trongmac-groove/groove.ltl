﻿using Groove.LTL.Domain.Customer.Commands;

namespace Groove.LTL.Domain.Customer.Validations
{
    /// <summary>
    /// Delete Customer Validation Class
    /// </summary>
    /// <seealso cref="Groove.LTL.Domain.Customer.Validations.CustomerValidation{DeleteCustomerCommand}" />
    public class DeleteCustomerValidation : CustomerValidation<DeleteCustomerCommand>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeleteCustomerValidation"/> class.
        /// </summary>
        public DeleteCustomerValidation()
        {
            ValidateId();
        }
    }
}
